const express = require('express');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');
const bahnQL = require('./src/data/server/db-graphql');

const app = express();
app.use(express.static('build'));

app.use('/graphql', cors('http://localhost:3000'), graphqlHTTP({
    schema: bahnQL.schema,
    rootValue: bahnQL.root,
    graphiql: true,
}));

app.get('/', (req, res) => {
    return res.sendFile('index.html');
});

// set the port of our application
// process.env.PORT lets the port be set by Heroku
const port = process.env.PORT || 8080;

app.listen(port, () => console.log(`now browse to localhost:${port}/graphql`));