#DB Station Info

Enables you to search a railway station from Deutsche Bahn and displays 
information about its layout, features and contact data.  
Also shows a current timetable of arriving and departing trains from that station.
 
TODO:
* Add some fancy interactivity
* Add geolocation search

This project uses the [1BahnQL API](https://github.com/dbsystel/1BahnQL) for GraphQL calls,    
[grommet](https://grommet.github.io/) as a base for React components and  
was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).