import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from 'grommet/components/Table';
import TableHeader from 'grommet/components/TableHeader';

import TimeTableRow from './TimeTableRow';

export default class TimeTable extends Component {
    static propTypes = {
        trains: PropTypes.object
    };

    render () {
        const timeTableLabels = [
            'Zeit',
            'Richtung',
            'Zug',
            'Gleis'
        ];

        if (!this.props.trains) {
            return null
        }

        return (
            <Table>
                <TableHeader
                    labels={timeTableLabels}
                />
                <tbody>
                    {this.props.trains.map(train => <TimeTableRow {...train} />)}
                </tbody>
            </Table>
        );
    }
}