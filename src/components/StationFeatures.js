import React from 'react';
import PropTypes from 'prop-types';
import Columns from 'grommet/components/Columns';
import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Heading from 'grommet/components/Heading';
import Status from 'grommet/components/icons/Status';

StationFeatures.propTypes = {
    station: PropTypes.object,
};

/**
 * Renders a list with information about the facilities available at a given train station
 * TODO: reduce boilerplate in list items
 * @param station
 */
export default function StationFeatures({station}) {
    let mobilityService = false;

    if (!station) {
        return;
    }

    if (
        station.hasMobilityService &&
        typeof station.hasMobilityService === 'string' &&
        station.hasMobilityService.toLowerCase().indexOf('ja') > -1
    ) {
        mobilityService = true;
    }

    return (
        <Columns
            justify='start'
            style={{marginBottom: '14px'}}
        >
            <Box margin='medium'>
                <Heading tag='h3'>Stationsausstattung</Heading>
                <List>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            DB-Lounge
                        </span>
                        <span>
                            <Status value={station.hasDBLounge ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Mobilitätsservice
                        </span>
                        <span
                            style={{
                                padding: '12px',
                                fontSize: '14px',
                                lineHeight: '1.43'
                            }}
                        >
                            {mobilityService && station.hasMobilityService}
                        </span>
                        <span>
                            <Status value={mobilityService ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Barrierefreier Zugang
                        </span>
                        <span>
                            <Status value={station.hasSteplessAccess === 'yes' ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            WLAN
                        </span>
                        <span>
                            <Status value={station.hasWiFi ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            WC
                        </span>
                        <span>
                            <Status value={station.hasPublicFacilities ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Schließfächer
                        </span>
                        <span>
                            <Status value={station.hasLockerSystem ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Fundbüro
                        </span>
                        <span>
                            <Status value={station.hasLostAndFound ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Reisebedarf (Einkaufsmöglichkeiten)
                        </span>
                        <span>
                            <Status value={station.hasTravelNecessities ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Bahnhofsmission
                        </span>
                        <span>
                            <Status value={station.hasRailwayMission ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                </List>
            </Box>
            <Box margin='medium'>
                <Heading tag='h3'>Mobilität</Heading>
                <List>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Automobilparkplatz
                        </span>
                        <span>
                            <Status value={station.hasParking ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Fahrad-Stellplätze
                        </span>
                        <span>
                            <Status value={station.hasBicycleParking ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Taxiwarteplatz
                        </span>
                        <span>
                            <Status value={station.hasTaxiRank ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            ÖPNV-Anbindung
                        </span>
                        <span>
                            <Status value={station.hasLocalPublicTransport ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                    <ListItem
                        justify='between'
                        separator='horizontal'
                        pad={{horizontal: 'none', vertical: 'small'}}
                    >
                        <span>
                            Mietwagen
                        </span>
                        <span>
                            <Status value={station.hasCarRental ? 'ok' : 'critical'} />
                        </span>
                    </ListItem>
                </List>
            </Box>
        </Columns>
    )
}