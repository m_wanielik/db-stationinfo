import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import TableRow from 'grommet/components/TableRow';
import MapLocationIcon from 'grommet/components/icons/base/MapLocation';

StationsTableRow.propTypes = {
    station: PropTypes.object,
};

export default function StationsTableRow({station}) {
    return (
        <TableRow>
            <td>
                <Link to={`station/${station.primaryEvaId}`}>
                    {station.name}
                </Link>
            </td>
            <td>
                <a
                    href={`https://maps.google.com/?q=${station.location.longitude},${station.location.latitude}`}
                    target='_blank'
                >
                    <MapLocationIcon colorIndex='brand' />
                </a>
            </td>
            <td>
                {station.primaryEvaId}
            </td>
        </TableRow>
    )
}