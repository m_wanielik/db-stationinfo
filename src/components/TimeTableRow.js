import React from 'react';
import PropTypes from 'prop-types';
import TableRow from 'grommet/components/TableRow';

TimeTableRow.propTypes = {
    type: PropTypes.string,
    trainNumber: PropTypes.string,
    platform: PropTypes.string,
    time: PropTypes.string,
    stops: PropTypes.array,
};

export default function TimeTableRow({type, trainNumber, platform, time, stops}) {
    // TODO: on heroku this leads to times being two hours earlier as the server is probably in Ireland
    const localeTime = new Date(Date.parse(time));

    return (
        <TableRow>
            <td>
                {localeTime.toLocaleTimeString()}
            </td>
            <td>
                {stops[0]}
            </td>
            <td>
                {type} {trainNumber}
            </td>
            <td>
                {platform}
            </td>
        </TableRow>
    )
}