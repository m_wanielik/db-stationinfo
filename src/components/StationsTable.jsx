import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo';
import stationsQuery from '../data/stations';
import Table from 'grommet/components/Table';
import Spinning from 'grommet/components/icons/Spinning';

import StationsTableRow from './StationsTableRow';

export class StationsList extends Component {
    static propTypes = {
        data: PropTypes.shape({
            loading: PropTypes.bool,
            error: PropTypes.object,
            search: PropTypes.object,
        }),
        searchTerm: PropTypes.string,
    };

    render () {
        let stationsResult = <tr><td colSpan={3}>Keine Suchanfrage gestartet</td></tr>;

        if (this.props.data) {
            if (this.props.data.loading) {
                stationsResult = <tr><td colSpan={3}><Spinning /> Lade Daten...</td></tr>;
            }

            if (this.props.data.error) {
                stationsResult = <tr><td colSpan={3}>Leider ist bei der Suchanfrage ein Fehler aufgetreten</td></tr>;
            }

            if (this.props.data.search && this.props.data.search.stations) {
                stationsResult = this.props.data.search.stations.map(station =>
                    <StationsTableRow key={station.primaryEvaId} station={station} />
                );
            }

        }

        return (
            <Table>
                <thead>
                    <tr>
                        <th>
                            Stationsname
                        </th>
                        <th>
                            Ort
                        </th>
                        <th>
                            Kennnummer
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {stationsResult}
                </tbody>
            </Table>
        );
    }
}

export default graphql(stationsQuery, {
    options: ({searchTerm}) => ({
        variables: {
            searchTerm: searchTerm
        },
    }),
    skip: props => {
        return !props.searchTerm.length
    },
})(StationsList);