import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Link,
    Route,
} from 'react-router-dom';
import ApolloClient, { createNetworkInterface } from 'apollo-client'
import { ApolloProvider } from 'react-apollo'

import GrommetApp from 'grommet/components/App';
import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import StationFinder from './StationFinder';
import Station from './Station';

const client = new ApolloClient({
    networkInterface: createNetworkInterface({ uri: `//${global.location.host}/graphql`}),
});

class App extends Component {
    render () {
        return (
            <ApolloProvider client={client}>
                <Router>
                    <GrommetApp>
                        <Header
                            colorIndex='brand'
                            pad={{horizontal: 'small', vertical: 'medium'}}
                        >
                            <Title>
                                <Link to='/'>
                                    DB Stations-Info
                                </Link>
                            </Title>
                        </Header>
                        <Route exact={true} path='/' component={StationFinder} />
                        <Route path='/station/:stationId' component={Station} />
                    </GrommetApp>
                </Router>
            </ApolloProvider>
        );
    }
}

export default App;
