import gql from 'graphql-tag';
import {timetable} from './timetable'

const stationsQuery = gql`
    query stationsQuery($searchTerm: String){
        search(searchTerm: $searchTerm) {
            stations {
                name,
                primaryEvaId,
                location {
                    latitude,
                    longitude,
                }             
            }
        }
    }   
`;

export const stationQuery = gql`
    query stationQuery($evaId: Int){
        stationWith(evaId: $evaId) {            
            primaryEvaId,
            stationNumber,
            primaryRil100,
            name,
            location {
                latitude,
                longitude,
            },
            category,
            priceCategory,
            hasParking,
            hasBicycleParking,
            hasLocalPublicTransport,
            hasPublicFacilities,
            hasLockerSystem,
            hasTaxiRank,
            hasTravelNecessities,
            hasSteplessAccess,
            hasMobilityService,
            federalState,
            regionalArea {
                number,
                name,
                shortName,
            },
            mailingAddress {
                city,
                zipcode,
                street,
            },
            hasWiFi,
            hasTravelCenter,
            hasRailwayMission,
            hasDBLounge,
            hasLostAndFound,
            hasCarRental,
            picture {
                url
            },
            timetable ${timetable}
        }
    }   
`;

export default stationsQuery;