const schema = require('./schema.js');

const ParkingspaceLoader = require('1BahnQL/Parkingspace/ParkingspaceLoader');
const FlinksterLoader = require('1BahnQL/Flinkster/FlinksterLoader');
const StationLoader = require('1BahnQL/Station/StationLoader');
const OperationLocationLoader = require('1BahnQL/OperationLocation/OperationLocationLoader');
const TravelCenterLoader = require('1BahnQL/TravelCenter/TravelCenterLoader');
const TimetableLoader = require('1BahnQL/Timetable/TimetableLoader.js');
const FacilityLoader = require('1BahnQL/Facility/FacilityLoader.js');
const StationPictureLoader = require('1BahnQL/StationPicture/StationPictureLoader');

const ParkingspaceService = require('1BahnQL/Parkingspace/ParkingspaceService');
const FlinksterService = require('1BahnQL/Flinkster/FlinksterService');
const OperationLocationService = require('1BahnQL/OperationLocation/OperationLocationService');
const StationService = require('1BahnQL/Station/StationService');
const NearbyStationService = require('1BahnQL/Station/NearbyStationsService');
const TravelCenterService = require('1BahnQL/TravelCenter/TravelCenterService');
const FacilityService = require('1BahnQL/Facility/FacilityService.js');
const RoutingService = require('1BahnQL/Routing/RoutingService.js');
const TimetableService = require('1BahnQL/Timetable/TimetableService.js');
const TrackService = require('1BahnQL/Platforms/TrackService.js');
const StationPictureService = require('1BahnQL/StationPicture/StationPictureService');

const StationRelationships = require('1BahnQL/Station/StationRelationships');
const ParkingspaceRelationships = require('1BahnQL/Parkingspace/ParkingspaceRelationships');
const RouteRelationships = require('1BahnQL/Routing/RouteRelationships');

const NearbyQuery = require('1BahnQL/NearbyQuery');

// --------- //

const APIToken = process.env.DBDeveloperAuthorization;

// Loader
const parkingspaceLoader = new ParkingspaceLoader(APIToken);
const stationLoader = new StationLoader(APIToken);
const timetableLoader = new TimetableLoader(APIToken);
const operationLocationLoader = new OperationLocationLoader(APIToken);
const travelCenterLoader = new TravelCenterLoader(APIToken);
const facilityLoader = new FacilityLoader(APIToken);
const flinksterLoader = new FlinksterLoader(APIToken);
const stationPictureLoader = new StationPictureLoader(APIToken);

// Services
const parkingspaceService = new ParkingspaceService(parkingspaceLoader);
const operationLocationService = new OperationLocationService(operationLocationLoader);
const stationService = new StationService(stationLoader);
const nearbyStationService = new NearbyStationService(stationService);
const travelCenterService = new TravelCenterService(travelCenterLoader);
const facilityService = new FacilityService(facilityLoader);
const routingService = new RoutingService();
const flinksterService = new FlinksterService(flinksterLoader);
const timetableServcie = new TimetableService(timetableLoader);
const trackService = new TrackService();
const stationPictureService = new StationPictureService(stationPictureLoader);

// Relationships
stationService.relationships = new StationRelationships(parkingspaceService, facilityService, timetableServcie, trackService, stationPictureService);
parkingspaceService.relationships = new ParkingspaceRelationships(parkingspaceService, stationService);
routingService.relationships = new RouteRelationships(stationService);

// Queries
const root = {
    parkingSpace: args => parkingspaceService.parkingspaceBySpaceId(args.id),
    stationWith: args => stationService.stationByEvaId(args.evaId),
    search: args => ({
        stations: stationService.searchStations(args.searchTerm),
        operationLocations: operationLocationService.searchOperationLocations(args.searchTerm)
    }),
    nearby: args => new NearbyQuery(args.latitude, args.longitude, args.radius, nearbyStationService, parkingspaceService, flinksterService, travelCenterService),
};

const experimental = process.env.experimental;
if (experimental) {
    root.routing = (args) => {
        const routeSearch = routingService.routes(args.from, args.to);
        return routeSearch.then(options => [options[0]]);
    }
}

module.exports = {
    root,
    schema
};