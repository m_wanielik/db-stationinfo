export const timetable = `{
    nextArrivals {
        type,
	    trainNumber,
	    platform,
	    time,
	    stops
    },
    nextDepatures {
        type,
	    trainNumber,
	    platform,
	    time,
	    stops
    }
}`;