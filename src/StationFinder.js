import React, { Component }  from 'react';
import Section from 'grommet/components/Section';
import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import Search from 'grommet/components/Search';

import StationsTable from './components/StationsTable';

export default class StationFinder extends Component {
    constructor (props) {
        super(props);
        this.state = {
            searchTerm: '',
        };
        this.setSearchTerm = this.setSearchTerm.bind(this);
    }

    setSearchTerm (event) {
        this.setState({
            searchTerm: event.target.value
        });
    }

    render () {
        return (
            <Section>
                <Box pad={{horizontal: 'small', vertical: 'medium'}}>
                    <Heading>Stationsübersicht</Heading>
                </Box>
                <Box pad={{horizontal: 'small', vertical: 'medium'}}>
                    <Search
                        ref='stationSearch'
                        placeHolder='Stationsname...'
                        inline={true}
                        onDOMChange={this.setSearchTerm}
                    />
                </Box>
                <Box pad={{horizontal: 'none', vertical: 'medium'}}>
                    <StationsTable searchTerm={this.state.searchTerm}/>
                </Box>
            </Section>
        )
    }
}
