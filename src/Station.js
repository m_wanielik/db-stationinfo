import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo';
import {stationQuery} from './data/stations';
import Section from 'grommet/components/Section';
import Box from 'grommet/components/Box';
import Split from 'grommet/components/Split';
import Tabs from 'grommet/components/Tabs';
import Tab from 'grommet/components/Tab';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Image from 'grommet/components/Image';
import {ListPlaceholder} from 'grommet-addons';
import MapLocationIcon from 'grommet/components/icons/base/MapLocation';

import StationFeatures from './components/StationFeatures';
import TimeTable from './components/TimeTable';

export class Station extends Component {
    static propTypes = {
        data: PropTypes.shape({
            loading: PropTypes.bool,
            error: PropTypes.object,
            stationWith: PropTypes.object,
        }),
    };

    render () {
        const {
            data: {
                loading,
                error,
                stationWith: station,
            }
        } = this.props;

        if (loading) {
            return (
                <Section>
                    <ListPlaceholder />
                </Section>
            )
        }

        if (error && !station) {
            return (
                <Section>
                    <Box>Leider ist beim Laden der Daten ein Fehler aufgetreten...</Box>
                </Section>
            )
        }

        return (
            <Section>
                <Box
                    pad={{horizontal: 'small', vertical: 'medium'}}
                >
                    <Heading>{station.name}</Heading>
                    <Box
                        direction='row'
                        justify='between'
                    >
                        {station.picture && station.picture.url && <Image src={station.picture.url} full='vertical' />}
                        <Paragraph>
                            <Heading tag='h4'>Adresse</Heading>
                            {station.name}
                            <br />
                            {station.mailingAddress.street}
                            <br />
                            {station.mailingAddress.zipcode} {station.mailingAddress.city}
                            <br />
                            {station.federalState}
                            <br /><br />
                            <a
                                href={`https://maps.google.com/?q=${station.location.longitude},${station.location.latitude}`}
                                target='_blank'
                            >
                                <MapLocationIcon colorIndex='brand' /> Auf Google Maps
                            </a>
                        </Paragraph>
                        <Paragraph>
                            <Heading tag='h4'>Identifikationsdaten</Heading>
                            EVA-Nummer: {station.primaryEvaId}
                            <br />
                            Ril 100: {station.primaryRil100}
                            <br />
                            Stationsnummer: {station.stationNumber}
                            <br />
                            Region: {station.regionalArea.name} ({station.regionalArea.number})
                        </Paragraph>
                    </Box>
                </Box>
                <Tabs>
                    <Tab title='Über die Station'>
                        <StationFeatures station={station} />
                    </Tab>
                    <Tab title='Fahrpläne'>
                        <Split showOnResponsive='both' fixed='false'>
                            <Box margin={{top: 'medium', right: 'medium'}}>
                                <Heading tag='h3'>Ankunft</Heading>
                                <TimeTable trains={station.timetable.nextArrivals} />
                            </Box>
                            <Box margin={{top: 'medium', left: 'medium'}}>
                                <Heading tag='h3'>Abfahrt</Heading>
                                <TimeTable trains={station.timetable.nextDepatures} />
                            </Box>
                        </Split>
                    </Tab>
                </Tabs>
            </Section>
        );
    }
}

export default graphql(stationQuery, {
    options: ({match: {params: {stationId}}}) => ({
        variables: {
            evaId: stationId
        },
    }),
})(Station);
